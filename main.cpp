#include <iostream>
#include <string>
#include <sstream>
#include <textbooks/book.hpp>
#include <textbooks/library.hpp>
#include <courses/course_list.hpp>

int main()
{
	textbooks::Library library;
	courses::Course_list course_list;
	std::string line;

	while (getline(std::cin, line))
	{
		// http://stackoverflow.com/a/7653762/5415895
		try{	
			std::stringstream ss(line);
			std::string arg;
			ss >> arg;

			if (arg == "B")
			{
				std::string isbn;
				std::string title;
				ss >> isbn;
				getline(ss, title);
				library.add_book(title, isbn);
			}
			else if (arg == "D")
			{
				std::string isbn;
				std::string characteristic;
				std::string value;
				ss >> isbn >> characteristic;
				getline(ss, value);
				
				if (characteristic == "A")
					library.set_author(isbn, value);
				else if (characteristic == "E")
					library.set_edition(isbn, stoi(value));
				else if (characteristic == "D")
					library.set_date(isbn, value);
				else
					std::cout << "Please enter a valid option." << std::endl;
			}
			else if (arg == "M")
			{
				std::string isbn;
				double cost;
				char format;
				ss >> isbn >> cost >> format;

				library.add_price(isbn, cost, format);
			}
			else if (arg == "C")
			{
				std::string department_code;
				std::string course_number;
				std::string  name;
				ss >> department_code >> course_number;
				getline(ss, name);

				course_list.add_course(department_code, course_number, name);
			}
			else if (arg == "A")
			{
				std::string isbn;
				std::string code;
				std::string number;
				std::string section;
				char status;
				ss >> isbn >> code >> number >> section >> status;

				textbooks::Book book = library.get_book(isbn);
				course_list.assign_book(code, number, section, book, status);
			}
			else if (arg == "GC")
			{
				std::string code;
				std::string number;
				ss >> code >> number;
				
				std::cout << course_list.get_course_books(code, number) << std::endl;
			}
			else if (arg == "GS")
			{
				std::string code;
				std::string number;
				std::string section;
				ss >> code >> number >> section;

				std::cout << course_list.get_section_books(code, number, section) << std::endl;
			}
			else if (arg == "PB")
			{
				if (library.empty())
					std::cout << "No books!" << std::endl;
				else
				{
					std::cout << library.get_all_books() << std::endl;
				}
			}
			else if (arg == "PC")
			{
				std::cout << course_list.get_all_courses() << std::endl;
			}
			else if (arg == "PY")
			{
				std::string date;
				ss >> date;

				std::cout << library.get_books_since(date) << std::endl;
			}
			else if (arg == "PD")
			{
				std::string code;
				ss >> code;
				
				std::cout << course_list.get_department_books(code) << std::endl;
			}
			else if (arg == "PM")
			{
				std::string code;
				ss >> code;

				std::pair<double, double> extrema = course_list.get_extrema(code);
				std::cout << "Max: " << extrema.first << ", Min: " << extrema.second << std::endl;
			}
			else
			{
				std::cout << "Please enter a valid option." << std::endl;
			}
		} // !try
		catch (const std::exception& e) 
		{ 
			std::cout << e.what() << std::endl;
		}
		catch (const char* s)
		{
			std::cout << s << std::endl;
		}
		catch (...)
		{
			std::cout << "Something bad happened." << std::endl;
		}
	} // !while
}