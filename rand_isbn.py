# Generate random ISBN numbers.

import random

res = [0]*13
for i in range(0, len(res)):
	res[i] = random.randrange(0,10)

print ''.join(map(str,res))