# Individual Assignment
CSCE 315 [individual assignment](https://www.dropbox.com/s/mvo7ce9j9q048wk/IndividualAssignment.pdf?dl=0).

## Instructions
You need [CMake](https://cmake.org/) to compile it.
In the root directory, run
```
mkdir _build
cd _build
cmake ../
make
```
and then run with `./main` or with Visual Studio.