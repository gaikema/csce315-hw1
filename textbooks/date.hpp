#ifndef DATE_HPP
#define DATE_HPP

#include <string>
#include <iostream>

namespace textbooks
{

// Simple date class.
class Date
{
	int _month;
	int _year;

public:
	Date(int m, int y);
	// Constructs a date given a string formatted like "MM/YYYY".
	Date(std::string date_string);

	int get_year();
	int get_month();

	friend std::ostream& operator<<(std::ostream& os, const Date& date);
	friend bool operator<(const Date& d1, const Date& d2);
	friend bool operator==(const Date& d1, const Date& d2);
	friend bool operator>=(const Date& d1, const Date& d2);
};

}

#endif