#include "date.hpp"
#include <sstream>
#include <iomanip>

namespace textbooks
{

Date::Date(int m, int y)
{
	_month = m;
	_year = y;
}

Date::Date(std::string date_string)
{
	std::stringstream ss(date_string);
	ss >> _month;
	// Ignore '/'
	ss.ignore();
	ss >> _year;
}

int Date::get_year()
{
	return _year;
}

int Date::get_month()
{
	return _month;
}

std::ostream& operator<<(std::ostream& os, const Date& date)
{
	// http://stackoverflow.com/a/1714538/5415895
	os << std::setfill('0') << std::setw(2) << date._month << "/" << std::setw(4) << date._year;

	return os;
}

bool operator<(const Date& d1, const Date& d2)
{
	if (d1._year != d2._year)
		return d1._year < d2._year;
	else
		return d1._month < d2._month;
}

bool operator==(const Date& d1, const Date& d2)
{
	return d1._year == d2._year && d1._month == d2._month;
}

bool operator>=(const Date& d1, const Date& d2)
{
	return !(d1 < d2);
}

}