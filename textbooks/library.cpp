#include "library.hpp"
#include <sstream>

namespace textbooks
{

Library::Library() {}

void Library::add_book(std::string title, std::string isbn)
{
	Book b(title, isbn);
	_books.insert({isbn, b});
}

void Library::set_author(std::string isbn, std::string author)
{
	if (_books.find(isbn) == _books.end())
		throw "Book does not exist";
	_books.at(isbn).set_author(author);
}

void Library::set_edition(std::string isbn, int edition)
{
	if (_books.find(isbn) == _books.end())
		throw "Book does not exist";	
	_books.at(isbn).set_edition(edition);
}

void Library::set_date(std::string isbn, std::string date)
{
	if (_books.find(isbn) == _books.end())
		throw "Book does not exist";
	_books.at(isbn).set_date(date);
}

void Library::add_price(std::string isbn, double price, char format)
{
	if (_books.find(isbn) == _books.end())
		throw "Book does not exist";	
	_books.at(isbn).add_price(price, format);
}

Book Library::get_book(std::string isbn)
{
	if (_books.find(isbn) == _books.end())
		throw "Book does not exist.";
	return _books.at(isbn);
}

bool Library::empty()
{
	return _books.empty();
}

std::string Library::get_all_books()
{
	std::stringstream ss;
	for (std::unordered_map<std::string, Book>::iterator it = _books.begin(); it != _books.end(); it++)
	{
		ss << it->second << std::endl;
	}

	return ss.str();
}

std::string Library::get_books_since(std::string date)
{
	if (_books.empty())
		throw "There are no books.";

	Date benchmark(date);
	std::stringstream ss;
	for (std::unordered_map<std::string, Book>::iterator it = _books.begin(); it != _books.end(); it++)
	{
		if (it->second.has_date() && it->second.get_date() >= benchmark)
		{
			ss << it->second << std::endl;
		}
	}

	return ss.str();
}

}