/*
	Header-only file containing a simple Cost class.
*/

#ifndef COST_HPP
#define COST_HPP

#include <iostream>
#include <iomanip>

namespace textbooks
{

enum Book_type {NEW, USED, RENTED, ELECTRONIC};

struct Cost
{
	double price;
	Book_type type;

	// http://stackoverflow.com/a/476777/5415895
	friend std::ostream& operator<<(std::ostream& os, const Cost& cst)
	{
		// http://www.cplusplus.com/reference/iomanip/setprecision/
		os << "$" << std::setprecision(2) << cst.price;
		return os;
	}

	friend bool operator<(Cost& c1, Cost& c2)
	{
		return c1.price < c2.price;
	}

	friend bool operator==(Cost& c1, Cost& c2)
	{
		return c1.price == c2.price;
	}

};

}

#endif