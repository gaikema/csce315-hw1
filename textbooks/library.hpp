#ifndef LIBRARY_HPP
#define LIBRARY_HPP

#include <unordered_map>
#include <string>
#include "book.hpp"

namespace textbooks
{

// A Library is a container of books.
class Library
{
	// Map isbn to textbook.
	std::unordered_map<std::string, Book> _books;

public:
	Library();

	// Insert a book.
	void add_book(std::string title, std::string isbn);

	// Set the author of a specific book.
	void set_author(std::string isbn, std::string author);
	// Set the edition of a specific book.
	void set_edition(std::string isbn, int edition);
	// Set the date of a specific book.
	void set_date(std::string isbn, std::string date);

	// Adds a price to the book.
	void add_price(std::string isbn, double price, char format);

	// Look up a book by ISBN.
	Book get_book(std::string isbn);

	// Returns true if the library has no books.
	bool empty();
	// Return a string containing all the books.
	std::string get_all_books();

	// Returns a string of all books since a certain date.
	std::string get_books_since(std::string date);
};

}

#endif