#include "book.hpp"
#include <sstream>
#include <limits>
#include <algorithm>
#include <utility>

namespace textbooks
{

Book::Book(std::string title, std::string isbn):
	_title(title),
	_isbn(isbn)
{
	_isbn.resize(13);
}

Book::~Book()
{
}

void Book::set_author(std::string author)
{
	_author.reset(new std::string(author));
}

void Book::set_edition(int edition)
{
	_edition.reset(new int(edition));
}

void Book::set_date(std::string date)
{
	//Date d(date);
	_publication_date.reset(new Date(date));
}

void Book::add_price(double price, char format)
{
	Cost cost;
	cost.price = price;
	switch (format)
	{
		case 'N':
			cost.type = Book_type::NEW;
			break;
		case 'U':
			cost.type = Book_type::USED;
			break;
		case 'R':
			cost.type = Book_type::RENTED;
			break;
		case 'E':
			cost.type = Book_type::ELECTRONIC;
			break;
		default:
			throw "Invalid format.";
			break;
	}

	_costs.insert({cost.type, cost});
}

bool Book::has_date()
{
	if (_publication_date)
		return true;
	else
		return false;
}

Date Book::get_date()
{
	if (_publication_date)
		return *_publication_date;
	else
		throw "Date undefined.";
}

std::pair<double, double> Book::get_max_min_price()
{
	if (_costs.empty())
		throw "No prices.";
	
	double current_min = std::numeric_limits<double>::max();
	double current_max = std::numeric_limits<double>::min();
	for (std::map<Book_type, Cost>::iterator it = _costs.begin(); it != _costs.end(); it++)
	{
		current_min = std::min(current_min, it->second.price);
		current_max = std::max(current_max, it->second.price);
	}

	return std::make_pair(current_max, current_min);
}

Book &Book::operator=(const Book &book)
{
	_isbn = book._isbn;
	_title = book._title;
	_costs = book._costs;

	_author.reset(new std::string(*book._author));
	_edition.reset(new int(*book._edition));
	_publication_date.reset(new Date(*book._publication_date));
}

std::ostream& operator<<(std::ostream& os, const Book& book)
{
	os << "title: " << book._title << std::endl;
	os << "isbn: " << book._isbn << std::endl;
	if (book._author)
		os << "author: " << *book._author << std::endl;
	if (book._edition)
		os << "edition: " << *book._edition << std::endl;
	if (book._publication_date)
		os << "published: " << *book._publication_date << std::endl;
	if (!book._costs.empty())
	{
		os << "price: ";
		// Use const_iterator because this is a constant operation.
		for (std::map<Book_type, Cost>::const_iterator it = book._costs.begin(); it != book._costs.end(); it++)
		{
			os << it->second << ", ";
		}
		os << std::endl;
	}

	return os;
}

}