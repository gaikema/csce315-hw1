#ifndef BOOK_HPP
#define BOOK_HPP

#include <map>
#include <string>
#include <memory>
#include <iostream>
#include "date.hpp"
#include "cost.hpp"

namespace textbooks
{

class Book
{
protected:	
	std::string _isbn;
	std::string _title;

	// Set with D
	std::shared_ptr<std::string> _author;
	std::shared_ptr<int> _edition;
	std::shared_ptr<Date> _publication_date;

	// cost of the book.
	/*
		A map here is used instead of an unordered map because the keys are enum types,
		but since there are only 4 possible keys, the time complexity should be fine.
		http://www.cplusplus.com/forum/general/74363/

		I could have just mapped chars to doubles but too late now.
	*/
	std::map<Book_type, Cost> _costs;

public:
	Book(std::string title, std::string isbn);
	~Book();

	// Set the author of the book.
	void set_author(std::string author);
	// Set the edition of the book.
	void set_edition(int edition);
	// Set the date the book was published.
	void set_date(std::string date);

	// Add a price to the book.
	void add_price(double price, char format);

	// Returns if the date has been set.
	bool has_date();
	// Getter method for date.
	Date get_date();

	// Get the highest and lowest price of a book.
	std::pair<double, double> get_max_min_price();

	Book& operator=(const Book& book);

	friend std::ostream& operator<<(std::ostream& os, const Book& book);
};

}

#endif