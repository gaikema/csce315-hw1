#include "course.hpp"
#include <sstream>

namespace courses
{

Course::Course(std::string code, std::string number, std::string name):
	_department_code(code),
	_course_number(number),
	_name(name)
{
	_department_code.resize(4);
	_course_number.resize(3);
}

void Course::assign_textbook(std::string section, Textbook& textbook)
{
	section.resize(3);
	_sections[section].push_back(textbook);
}

std::string Course::get_sections()
{
	if (_sections.empty())
		throw "No sections";
	
	std::stringstream ss;
	for (auto it = _sections.begin(); it != _sections.end(); it++)
	{
		ss << "Section:" << it->first << std::endl;
		std::list<Textbook>& lst = it->second;
		if (!lst.empty())
		{
			for (auto jt = lst.begin(); jt != lst.end(); jt++)
			{
				ss << *jt << std::endl;
				std::cout << *jt << std::endl;
			}
		}

		std::cout << "In loop" << std::endl;
	}

	std::cout << "Out of loop." << std::endl;
	return ss.str();
}

std::string Course::get_section(std::string section)
{
	std::stringstream ss;
	if (_sections.find(section) == _sections.end())
		throw "This section does not exist!";
	
	auto books = _sections.at(section);
	for (auto it = books.begin(); it != books.end(); it++)
	{
		ss << *it << std::endl;
	}

	return ss.str();
}

std::list<Textbook> Course::get_all_books()
{
    std::list<Textbook> books;
    if (_sections.empty())
		return books;
	for (auto it = _sections.begin(); it != _sections.end(); it++)
	{
		books.insert(books.end(), it->second.begin(), it->second.end());
	}

	return books;
}

std::pair< std::vector<double>, std::vector<double> > Course::get_extrema()
{
	if (_sections.empty())
		throw "No sections";
	
	std::vector<double> mins;
	std::vector<double> maxes;
	for (auto it = _sections.begin(); it != _sections.end(); it++)
	{
		std::list<Textbook>& lst = it->second;
		for (auto jt = lst.begin(); jt != lst.end(); jt++)
		{
			std::pair<double, double> extrema = jt->get_book_extrema();
			mins.push_back(extrema.second);
			maxes.push_back(extrema.first);
		}
	}
	return std::make_pair(maxes, mins);
}

std::ostream& operator<<(std::ostream& os, const Course& course)
{
	os << course._department_code << " " << course._course_number << ": " << course._name;
	return os;
}

}