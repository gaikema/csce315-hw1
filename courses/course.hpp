#ifndef COURSE_HPP
#define COURSE_HPP

#include <string>
#include <unordered_map>
#include <list>
#include <iostream>
#include <vector>
#include <utility>
#include "textbook.hpp"

namespace courses
{

class Course
{
	std::string _department_code;
	std::string _course_number;
	std::string _name;

	// Map the section to the list of books assigned.
	std::unordered_map<std::string, std::list<Textbook>> _sections;
	
public:
	Course(std::string code, std::string number, std::string name);

	// Assign a textbook to a section.
	void assign_textbook(std::string section, Textbook& textbook);
	// Print the books for each section.
	std::string get_sections();
	// Print the books for a single section.
	std::string get_section(std::string section);
	// Return a list of all textbooks assigned to a course.
	std::list<Textbook> get_all_books();
	// Get the most expensive and least expensive textbooks assigned to a course (respectively).
	std::pair< std::vector<double>, std::vector<double> > get_extrema();

	friend std::ostream& operator<<(std::ostream& os, const Course& course);
};

}

#endif