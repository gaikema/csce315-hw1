#ifndef TEXTBOOK_HPP
#define TEXTBOOK_HPP

#include <memory>
#include <string>
#include <iostream>
#include <textbooks/book.hpp>
#include <utility>

namespace courses
{

enum Textbook_flag {REQUIRED, OPTIONAL};

class Textbook
{
	textbooks::Book _book;
	Textbook_flag _textbook_flag;

public:
	Textbook(textbooks::Book& book, char flag);
	
	// Returns if a textbook is required or optional.
	Textbook_flag get_status();
	// Max and Min prices, respectively.
	std::pair<double, double> get_book_extrema();

	friend std::ostream& operator<<(std::ostream& os, const Textbook& textbook);
};

}

#endif