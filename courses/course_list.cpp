#include "course_list.hpp"
#include <sstream>
#include <list>
#include <vector>
#include <numeric>

namespace courses
{

std::string Course_list::convert_to_key(std::string code, std::string number)
{
	code.resize(4);
	number.resize(3);
	return code + " " + number;
}

std::string Course_list::get_first_word(std::string str)
{
	std::stringstream ss(str);
	std::string res;
	ss >> res;
	return res;
}

double Course_list::vector_mean(std::vector<double> vec)
{
	return std::accumulate(vec.begin(), vec.end(), 0)/vec.size();
}

void Course_list::add_course(std::string code, std::string number, std::string name)
{
	Course course(code, number, name);
	std::string key = convert_to_key(code, number);
	_courses.insert({key, course});
}

void Course_list::assign_book(std::string code, std::string number, std::string section, textbooks::Book book, char status)
{
	std::string key = convert_to_key(code, number);
	if (_courses.find(key) == _courses.end())
		throw "Course does not exist!";

	Textbook textbook(book, status);
	_courses.at(key).assign_textbook(section, textbook);
}

std::string Course_list::get_course_books(std::string code, std::string number)
{
	std::string key = convert_to_key(code, number);
	if (_courses.find(key) == _courses.end())
		throw "Course does not exist.";
	return _courses.at(key).get_sections();
}

std::string Course_list::get_section_books(std::string code, std::string number, std::string section)
{
	std::string key = convert_to_key(code, number);
	if (_courses.find(key) == _courses.end())
		throw "Course does not exist.";
	return _courses.at(key).get_section(section);
}

std::string Course_list::get_all_courses()
{
	std::stringstream ss;
	
	if (_courses.empty())
		throw "No courses.";
	for (std::unordered_map<std::string, Course>::iterator it = _courses.begin(); it != _courses.end(); it++)
	{
		ss << it->first << std::endl;
	}

	return ss.str();
}

std::string Course_list::get_department_books(std::string code)
{
	if (_courses.empty())
		throw "No courses.";
	
	std::list<Textbook> textbooks;
	for (auto it = _courses.begin(); it != _courses.end(); it++)
	{
		if (get_first_word(it->first) == code)
		{
			std::list<Textbook> lst = it->second.get_all_books();
			textbooks.insert(textbooks.end(), lst.begin(), lst.end());
		}
	}

	std::stringstream ss;
	for (std::list<Textbook>::iterator it = textbooks.begin(); it != textbooks.end(); it++)
	{
		ss << *it << std::endl;
	}

	return ss.str();
}

std::pair<double, double> Course_list::get_extrema(std::string code)
{
	if (_courses.empty())
		throw "No courses.";
	
	std::vector<double> maxes;
	std::vector<double> mins;
	for (auto it = _courses.begin(); it != _courses.end(); it++)
	{
		if (get_first_word(it->first) == code)
		{
			std::pair< std::vector<double>, std::vector<double> > res = it->second.get_extrema();
			maxes.insert(maxes.end(), res.first.begin(), res.first.end());
			mins.insert(mins.end(), res.second.begin(), res.second.end());
		}
	}

	return std::make_pair(vector_mean(maxes), vector_mean(mins));
}

}