#ifndef COURSE_LIST_HPP
#define COURSE_LIST_HPP

#include "course.hpp"
#include <unordered_map>
#include <string>

namespace courses
{

// Container for all the defined courses.
class Course_list
{
	// Map course code + number to Courses.
	std::unordered_map<std::string, Course> _courses;

	// Converts a department code and a number to a key for the map.
	static std::string convert_to_key(std::string code, std::string number);
	// Get the word in a string with multiple words (seperated by spaces).
	static std::string get_first_word(std::string str);
	// Gets the average from a vector of doubles.
	double vector_mean(std::vector<double> vec);

public:
	// Inserts a course to the list.
	void add_course(std::string code, std::string number, std::string name);
	// Assigns a textbook to a section of a course.
	void assign_book(std::string code, std::string number, std::string section, textbooks::Book book, char status);
	// Get all the books for a course.
	std::string get_course_books(std::string code, std::string number);
	// Get all the books for a section of a course.
	std::string get_section_books(std::string code, std::string number, std::string section);

	// Get all the books from a department.
	std::string get_department_books(std::string code);

	// Return a string containing all the courses.
	std::string get_all_courses();

	// Returns the average max price and average min price of all the books assigned to a certain department.
	std::pair<double, double> get_extrema(std::string code);
};

}

#endif