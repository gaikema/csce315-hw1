#include "textbook.hpp"

namespace courses
{

Textbook::Textbook(textbooks::Book& book, char flag): 
_book(book)
{
	if (flag == 'R')
		_textbook_flag = Textbook_flag::REQUIRED;
	else if (flag == 'O')
		_textbook_flag = Textbook_flag::OPTIONAL;
	else
		throw "Please specify if the book is required or optional.";
}

Textbook_flag Textbook::get_status()
{
	return _textbook_flag;
}

std::pair<double, double> Textbook::get_book_extrema()
{
	return _book.get_max_min_price();
}

std::ostream& operator<<(std::ostream& os, const Textbook& textbook)
{
	char flag = textbook._textbook_flag == Textbook_flag::REQUIRED ? 'R' : 'O';
	os << textbook._book; // No newline because it is in Book.
	os << flag << std::endl;
	return os;
}

}